import aiohttp
import discord
from dotenv import load_dotenv
import os

load_dotenv()
client = discord.Client()
discord_client_secret = os.getenv("DISCORD_CLIENT_SECRET")
spotify_client_id = os.getenv("SPOTIFY_CLIENT_ID")
spotify_client_secret = os.getenv("SPOTIFY_CLIENT_SECRET")
spotify_scopes = 'user-read-private user-read-email user-modify-playback-state'
spotify_token = os.getenv("SPOTIFY_TOKEN")
headers = {'Authorization': "Bearer " + spotify_token}

base_url = "https://api.spotify.com/v1"


@client.event
async def on_ready():
    print("Ready")


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith("!"):
        await handle_command(message)


async def handle_command(message):
    command = message.content[1:]
    if command.startswith("play"):
        await start_or_resume_playback(message)
    elif command.startswith("pause"):
        await pause_playback(message)
    elif command.startswith("next"):
        await skip_to_next_track(message)
    elif command.startswith("previous"):
        await skip_to_previous_track(message)
    elif command.startswith("volume"):
        parts = command.split()
        await set_volume(message, int(parts[1]))
    elif command.startswith("shuffle"):
        await set_shuffle(message)
    elif command.startswith("noshuffle"):
        await set_no_shuffle(message)
    elif command.startswith("add"):
        parts = command.split()
        await add_to_queue(message, parts[1])


async def start_or_resume_playback(message):
    await message.channel.send("Let us play!")
    await do_put(f"{base_url}/me/player/play")


async def pause_playback(message):
    await message.channel.send("Hold on for a minute!")
    await do_put(f"{base_url}/me/player/pause")


async def skip_to_next_track(message):
    await message.channel.send("Skipping this.")
    await do_post(f"{base_url}/me/player/next")


async def skip_to_previous_track(message):
    await message.channel.send("Let me hear that again.")
    await do_post(f"{base_url}/me/player/previous")


async def set_volume(message, volume):
    await message.channel.send("Changing volume.")
    if 0 <= volume <= 100:
        await do_post(f"{base_url}/me/player/previous?volume_percent={volume}")


async def set_shuffle(message):
    await message.channel.send("Everyday I'm shuffling.")
    await do_put(f"{base_url}/me/player/shuffle?state=true")


async def set_no_shuffle(message):
    await message.channel.send("Everyday I'm not shuffling.")
    await do_put(f"{base_url}/me/player/shuffle?state=false")


async def add_to_queue(message, track):
    await message.channel.send("Adding to queue!")
    await do_post(f"{base_url}/me/player/queue?uri=spotify:track:{track}")


async def do_put(url):
    async with aiohttp.ClientSession() as session:
        async with session.put(url, headers=headers) as response:
            print(f"For PUT url {url} received status {response.status}")


async def do_post(url):
    async with aiohttp.ClientSession() as session:
        async with session.post(url, headers=headers) as response:
            print(f"For POST url {url} received status {response.status}")

client.run(discord_client_secret)
